<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

$app->get('/types', 'getTypes');
$app->get('/species/:typeId', 'getSpecies');
// $app->get('/species', 'getSpecies');


function getTypes(){
    $sql = "SELECT * FROM type ORDER BY id asc";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $types = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $response = array('types' => $types);
        sendResponse(0, 'Success', $response);
    } catch(PDOException $e) {
        sendResponse(501, $e->getMessage(), null);
    }
}

function getSpecies($typeId) {
    $sql = "SELECT * FROM species ORDER BY id asc";
    if($typeId > 0) {
        $sql = "SELECT * FROM species WHERE typeId=:typeId ORDER BY id asc";
    }

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("typeId", $typeId);
        $stmt->execute();
        $species = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $response = array('species' => $species);
        sendResponse(0, 'Success', $response);
    } catch(PDOException $e) {
        sendResponse(501, $e->getMessage(), $response);
    }
}

function sendResponse($code, $description, $response) {
    if($response == null){
        $response = array(
            'code' => $code,
            'description' => $description,
            );
        echo json_encode($response);
    } else {
        $response['code'] = $code;
        $response['description'] = $description;
        echo json_encode($response);
    }
}

function datetime() {
    return date("Y-m-d H:i:s"); 
}

//Connect Database
function getConnection() {
    $dbhost="localhost";
    $dbuser="pippocom_cat";
    $dbpass="DjvEtqeY";
    $dbname= "pippocom_cat";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh -> exec("set names utf8");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}
 
$app->run();
 
?>